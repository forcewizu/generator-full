const ROOT = 'http://104.248.178.17/api';

const categoryId = 15;

const api = {
  posts: `${ROOT}/get-articles`,
}

export { categoryId };
export default api;