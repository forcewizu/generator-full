import http from 'axios';
import Format from './format';
import api, { categoryId } from './api';

const { truncate } = new Format();

class HttpService {

  _generateTruncate = (text, keyword, textLength, titleLength) => {
    return {
      text: truncate({
        str: text,
        length: textLength,
      }),
      keyword: truncate({
        str: keyword,
        length: titleLength,
      }),
    }
  }

  formatPost = (post) => {
    
    let newData = {};
    if (post.article) {
      const { article: { translate_text: text = '' }, keyword = '' } = post;
      newData = {
        truncated: [
          this._generateTruncate(text, keyword, 46, 36),
          this._generateTruncate(text, keyword, 56, 40),
          this._generateTruncate(text, keyword, 72, 54),
          this._generateTruncate(text, keyword, 98, 60),
          this._generateTruncate(text, keyword, 200, 110),
          this._generateTruncate(text, keyword, 82, 84),
          this._generateTruncate(text, keyword, 118, 98),
          this._generateTruncate(text, keyword, 128, 120),
          this._generateTruncate(text, keyword, 138, 140),
          this._generateTruncate(text, keyword, 32, 36),
          this._generateTruncate(text, keyword, 58, 40),
          this._generateTruncate(text, keyword, 68, 50),
          this._generateTruncate(text, keyword, 70, 56),
          this._generateTruncate(text, keyword, 70, 72),
          this._generateTruncate(text, keyword, 80, 82),
        ],
        truncatedSection: [
          this._generateTruncate(text, keyword, 44, 32),
          this._generateTruncate(text, keyword, 46, 36),
          this._generateTruncate(text, keyword, 48, 54),
          this._generateTruncate(text, keyword, 100, 70),
          this._generateTruncate(text, keyword, 200, 110),
          this._generateTruncate(text, keyword, 82, 84),
          this._generateTruncate(text, keyword, 118, 98),
          this._generateTruncate(text, keyword, 128, 120),
          this._generateTruncate(text, keyword, 170, 140),
          this._generateTruncate(text, keyword, 190, 150),
          this._generateTruncate(text, keyword, 200, 160),
          this._generateTruncate(text, keyword, 480, 220),
          this._generateTruncate(text, keyword, 580, 240),
          this._generateTruncate(text, keyword, 660, 300),
          this._generateTruncate(text, keyword, 680, 320),
        ],
        truncatedAside: [
          this._generateTruncate(text, keyword, 0, 0),
          this._generateTruncate(text, keyword, 0, 0),
          this._generateTruncate(text, keyword, 0, 0),
          this._generateTruncate(text, keyword, 0, 0),
          this._generateTruncate(text, keyword, 0, 0),
          this._generateTruncate(text, keyword, 28, 22),
          this._generateTruncate(text, keyword, 30, 24),
          this._generateTruncate(text, keyword, 32, 36),
          this._generateTruncate(text, keyword, 32, 38),
          this._generateTruncate(text, keyword, 36, 40),
          this._generateTruncate(text, keyword, 46, 48),
          this._generateTruncate(text, keyword, 68, 56),
          this._generateTruncate(text, keyword, 70, 66),
          this._generateTruncate(text, keyword, 72, 72),
          this._generateTruncate(text, keyword, 82, 82),
        ],
      }
    }
    const result = Object.assign({}, post, newData);
    return result;
  }
  
  getPosts = async () => {
    try {
      // const data = await http.get('db.json');
      // const result = data.map(this.formatPost);
      return [];
    } catch(e) {
      console.error('Somethinf wrong => ', e.response || e );
      throw e;
    }
  };
};

export default new HttpService();