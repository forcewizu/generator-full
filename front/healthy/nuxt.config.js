import _ from 'lodash';
import fse from 'fs-extra';
import formatPost from './plugins/formatPost';
import filterPost from './plugins/filterPost';
import formatCategory from './plugins/formatCategory';
import getCategoriesRoutes from './plugins/filterCategory';
import ampify from './plugins/ampify';
import setPrefix from './plugins/class-prefix';
import has from 'has';

const postPure = {
  id: null,
  keyword: '',
  article: {
    id: null,
    translate_text: ''
  },
  image: {
    id: null,
    name: '',
    link: ''
  },
  categories: {
    id: null,
    name: ''
  }
};

const params = {
  APP_COLOR: `#${process.env.MY_COLOR ? process.env.MY_COLOR : '009CBD'}`,
  APP_FONT: process.env.MY_FONT || "'GothamPro', 'Open Sans', 'sans-serif'",
  APP_FONT_SIZE: process.env.MY_FONT_SIZE | 16,
  APP_NUMBER: process.env.MY_NUMBER || 10,
  baseUrl: process.env.BASE_URL || 'http://localhost:3000',
};

console.table(params);

module.exports = {
  env: { ...params },
  generate: {
    routes() {
      return fse.readJson('./db.json').then(resp => {
        const formatedData = resp.map(formatPost).filter(filterPost).map(formatCategory);
        const posts = formatedData.map((post, idx, arr) => {
          return {
            route: '/' + post.route,
            payload: {
              post: Object.assign({}, postPure, post),
              posts: arr
            }
          };
        });
        const ampPosts = formatedData.map((post, idx, arr) => {
          return {
            route: '/amp/' + post.route,
            payload: {
              post: Object.assign({}, postPure, post),
              posts: arr
            }
          };
        });
        const categoriesList = getCategoriesRoutes(formatedData, params.APP_NUMBER);
        const home = {
          route: '/',
          payload: {
            posts: formatedData
          }
        };
        return [...categoriesList, ...posts, ...ampPosts, home ];
      });
    }
  },
  loading: {
    color: '#009cbd'
  },
  plugins: [
    '~/plugins/vue-components.js',
    { src: '~/plugins/formatPost.js', ssr: true }
  ],
  modules: ['@nuxtjs/style-resources'],
  styleResources: {
    scss: [
      '~/assets/scss/core/_mixins.scss',
      '~/assets/scss/core/_vars.scss',
      '~/assets/scss/core/_inheritance.scss'
    ]
  },
  head: {
    title: 'Health',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Health' }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  build: {
    /*
     ** Run ESLint on save
     */
    extend(config, { isDev, isClient, loaders  }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        });
      };
    }
  },
  hooks: {
    // This hook is called before saving the html to flat file
    'generate:page': (page) => {
      if (/^\/amp\//gi.test(page.route)) {
        page.html = ampify(page.html)
      }
      page.html = setPrefix(page.html);
    },
    // This hook is called before serving the html to the browser
    'render:route': (url, page, { req, res }) => {
      if (/^\/amp\//gi.test(url)) {
        page.html = ampify(page.html, url);
      }
      page.html = setPrefix(page.html);
    },
  }
};
