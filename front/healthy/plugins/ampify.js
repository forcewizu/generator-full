const ampScript = '<script async src="https://cdn.ampproject.org/v0.js"></script>';
const ampForm = '<script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>';
const ampSidebar = '<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>';
const ampBoilerplate = '<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>';
import styles from '../assets/js/amp-styles.js';

const ampify = (html, url) => {
  try {
    // Add ⚡ to html tag
    html = html.replace(/<html/gi, '<html ⚡');

    // Combine css into single tag
    const reStyles = /<style\b[^<]*(?:(?!<\/style>)<[^<]*)*<\/style>/gi;
    if (reStyles.test(html)) {
      html = html.replace(reStyles, (match) => {
        return (/amp-custom>/gi.test(match)) ? match : '';
      })
    }
    html = html.replace('</head>', `<style amp-custom>${styles}</style></head>`)

    // Remove preload and prefetch tags
    html = html.replace(/<link[^>]*rel="(?:preload|prefetch)?"[^>]*>/gi, '')

    // Remove amphtml tag
    const reAmp = /<link[^>]*rel="(?:amphtml)?"[^>]*>/gi;
    if (reAmp.test(html)) {
      html = html.replace(reAmp, '');
    }
    
    // Remove data attributes from tags
    const reTags = /\s*=>]*="[^"]*"|[^=>\s]*\)/gi;
    if (reTags.test(html)) {
      html = html.replace(reTags, '');
    }

    // Remove JS script tags except for ld+json
    const reJson = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi;
    if (reJson.test(html)) {
      html = html.replace(reJson, (match) => {
        return (/application\/ld\+json/gi.test(match)) ? match : ''
      })
    }

    // Replace img tags with amp-img
    const reImg = /<img([^>]*)>/gi;
    if (reImg.test(html)) {
      html = html.replace(reImg, (match, sub) => {
        return `<amp-img ${sub} layout=intrinsic></amp-img>`
      })
    }

    // Add AMP script before </head>
    html = html.replace('</head>', ampScript + ampSidebar + ampForm + ampBoilerplate + '</head>')

    
    return html
  } catch (e) {
    console.error(e);
  }
}

export default ampify;