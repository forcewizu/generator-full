const setPrefix = (html) => {
  const prefix = Date.now();
  html = html.replace(/-prfx/gi, `-${prefix}`);
  return html;
};

export default setPrefix;