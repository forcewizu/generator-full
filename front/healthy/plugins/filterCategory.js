import has from 'has';
import _ from 'lodash';

const filterPosts = ({ category }) => !!category;
const inclonePosts = (data) => {
  return _.uniqWith(
    data,
    ({ category: { id: arrId } }, { category: { id: othId } }) =>
      arrId === othId
  )
};

const getCategoriesRoutes = (formatedData, maxNumber) => {
  const filtered = formatedData.filter(filterPosts);
  const uncloned = inclonePosts(filtered);
  const categoriesList = uncloned.map(({ category }) => {
    const filteredByRoute = formatedData.filter((post) => {
      return has(post, 'category') && post.category.route === category.route;
    });
    const postsCategory = filteredByRoute.filter((a, idx) => Number(idx) <= Number(maxNumber));
    return {
      route: '/category/' + category.route,
      payload: {
        category: category,
        postsCategory,
        posts: formatedData,
      }
    };
  });
  return categoriesList;
}

const filterPostsByCategory = (formatedData, route, maxNumber) => {
  const filteredByRoute = formatedData.filter((post) => {
    return has(post, 'category') && post.category.route === route;
  });
  const postsCategory = filteredByRoute.filter((a, idx) => Number(idx) <= Number(maxNumber));
  return postsCategory;
}

export { filterPostsByCategory };
export default getCategoriesRoutes;