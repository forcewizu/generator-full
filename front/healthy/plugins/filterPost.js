import has from 'has';

const filterPost = (post) => {

  return has(post, 'article_id') && has(post, 'article') && post.article;
}

export default filterPost;