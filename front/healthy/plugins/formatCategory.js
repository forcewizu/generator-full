import { generateRouteLink } from '../utils';
import has from 'has';

const formatCategory = post => {
  let formatedPost = post;
  if (has(post, 'category') && has(post.category, 'name')) {
    formatedPost.category.route = generateRouteLink(formatedPost.category.name);
  }
  return formatedPost;
};

export default formatCategory;
