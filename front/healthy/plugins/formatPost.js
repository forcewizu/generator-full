import { generateRouteLink } from '../utils';

const formatPost = post => {
  const truncate = ({ type = 'text', str, length = 100, sufix = '...' }) => {
    let maxLength = length;
    const result = max => {
      if (Number(str.length) <= Number(max)) return str;
      return `${str.substring(0, max)}${sufix}`;
    };
    return result(maxLength);
  };

  const _generateTruncate = (text, keyword, textLength, titleLength, sufix = '...') => {
    return {
      text: truncate({
        str: text,
        length: textLength,
        sufix,
      }),
      keyword: truncate({
        str: keyword,
        length: titleLength,
        sufix,
      })
    };
  };

  let newData = {};
  if (post.article && post.article.translate_text) {
    const maxTextLength = 150;
    const titleLength = post.keyword_en.length;
    post.keyword = post.keyword_en;
    post.route = generateRouteLink(post.keyword_en || post.keyword);
    const {
      article: { translate_text: text = '' },
      keyword_en: keyword = ''
    } = post;
    newData = {
      sliderText: _generateTruncate(text, keyword, 150, 100, ''),
      truncated: [
        _generateTruncate(text, keyword, 46, 36),
        _generateTruncate(text, keyword, 56, 40),
        _generateTruncate(text, keyword, 72, 54),
        _generateTruncate(text, keyword, 98, 60),
        _generateTruncate(text, keyword, maxTextLength, 110),
        _generateTruncate(text, keyword, 82, 84),
        _generateTruncate(text, keyword, 118, 98),
        _generateTruncate(text, keyword, 128, 120),
        _generateTruncate(text, keyword, 138, 140),
        _generateTruncate(text, keyword, 32, 36),
        _generateTruncate(text, keyword, 58, 40),
        _generateTruncate(text, keyword, 68, 50),
        _generateTruncate(text, keyword, 70, 56),
        _generateTruncate(text, keyword, 70, 72),
        _generateTruncate(text, keyword, 80, 82)
      ],
      truncatedSection: [
        _generateTruncate(text, keyword, 44, 32),
        _generateTruncate(text, keyword, 46, 36),
        _generateTruncate(text, keyword, 48, 54),
        _generateTruncate(text, keyword, 100, 70),
        _generateTruncate(text, keyword, maxTextLength, 110),
        _generateTruncate(text, keyword, 82, 84),
        _generateTruncate(text, keyword, 118, 98),
        _generateTruncate(text, keyword, 128, 120),
        _generateTruncate(text, keyword, maxTextLength, 150),
        _generateTruncate(text, keyword, maxTextLength, 170),
        _generateTruncate(text, keyword, maxTextLength, 180),
        _generateTruncate(text, keyword, maxTextLength, 240),
        _generateTruncate(text, keyword, maxTextLength, 280),
        _generateTruncate(text, keyword, maxTextLength, titleLength),
        _generateTruncate(text, keyword, maxTextLength, titleLength),
      ],
      truncatedAside: [
        _generateTruncate(text, keyword, 0, 0),
        _generateTruncate(text, keyword, 0, 0),
        _generateTruncate(text, keyword, 0, 0),
        _generateTruncate(text, keyword, 0, 0),
        _generateTruncate(text, keyword, 0, 0),
        _generateTruncate(text, keyword, 28, 22),
        _generateTruncate(text, keyword, 30, 24),
        _generateTruncate(text, keyword, 32, 36),
        _generateTruncate(text, keyword, 32, 38),
        _generateTruncate(text, keyword, 36, 40),
        _generateTruncate(text, keyword, 46, 48),
        _generateTruncate(text, keyword, 68, 56),
        _generateTruncate(text, keyword, 70, 66),
        _generateTruncate(text, keyword, 72, 72),
        _generateTruncate(text, keyword, 82, 82)
      ]
    };
  }
  const result = Object.assign({}, post, newData);
  return result;
};

export default formatPost;
