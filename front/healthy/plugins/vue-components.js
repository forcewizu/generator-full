import Vue from 'vue';
import _ from 'lodash';

// import '../assets/css/font.css';
import '../assets/scss/style.scss';
import AppHeader from '../components/header/AppHeader';
import AppFooter from '../components/footer/AppFooter';
import AppMenu from '../components/common/AppMenu';
import AppAside from '../components/aside/AppAside';
import AppCard from '../components/common/AppCard';
import AppTopics from '../components/common/AppTopics';
import AppTopic from '../components/common/AppTopic';

import 'vue-instant/dist/vue-instant.css';
import VueInstant from 'vue-instant';
Vue.use(VueInstant);

Vue.component('AppHeader', AppHeader);
Vue.component('AppFooter', AppFooter);
Vue.component('AppMenu', AppMenu);
Vue.component('AppAside', AppAside);
Vue.component('AppCard', AppCard);
Vue.component('AppTopics', AppTopics);
Vue.component('AppTopic', AppTopic);

Vue.prototype.$lodash = _;