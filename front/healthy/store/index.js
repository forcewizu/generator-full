import _ from 'lodash';
import Format from '../assets/js/format';

const { lightenDarkenColor } = new Format();

export const state = () => ({
  post: {
    id: null,
    keyword: '',
    article: {
      id: null,
      translate_text: '',
    },
    image: {
      id: null,
      name: '',
      link: ''
    },
    category: {
        id: null,
        name: '',
        route: '',
    },
  },
  posts: [],
  variables: {
    MY_COLOR: process.env.APP_COLOR,
    MY_FONT: process.env.APP_FONT,
    MY_FONT_SIZE: process.env.APP_FONT_SIZE,
    MY_NUMBER: process.env.APP_NUMBER,
  },
})

export const getters = {
  styles: (state) => {
    const { MY_COLOR, MY_FONT, MY_FONT_SIZE } = state.variables;
    return {
      '--base-size': `${MY_FONT_SIZE}px`,
      '--base-font': MY_FONT,
      '--lighten-bg-color': lightenDarkenColor(MY_COLOR, 20),
      '--darken-bg-color': lightenDarkenColor(MY_COLOR, -10),
      '--bg-color': MY_COLOR,
    }
  },
  isPosts: (state) => {
    return !!state.posts.length;
  },
  getTopics: (state) => {
    return state.posts.map((post) => {
      return {
        id: post.id,
        route: post.route,
        keyword: post.keyword,
      };
    }) 
  },
  getFillPosts: (state) => {
    return state.posts.map((post) => {
      let result = {...post};
      if (result.article === 'null' || !result.article) result.article = {...state.post.article};
      return Object.assign({}, state.post, result)
    });
  },
  getPostsByCategory: (state, getters) => (categoryId) => {
    const posts = getters.getFillPosts.filter(({ category: { route } }) => route === categoryId);
    return posts.filter((e, idx) => idx <= state.variables.MY_NUMBER);
  },
  getCategoryById: (state, getters) => (categoryId) => {
    return getters.getCategories.find(({route}) => route === categoryId);
  },
  getCategories: (state, getters) => {
    let sections = [];
    if (getters.getFillPosts.length) {
      const filtered = _.uniqWith(getters.getFillPosts, ({ category: { id: arrId } }, { category: { id: othId } }) => arrId === othId);
      sections = filtered.map(({ category }) => category);
    }
    return sections.filter(({ name }) => name && name !== 'null');
  },
  getPostsSlider: (state, getters) => {
    return getters.getFillPosts.filter((post, index) => index < 5);
  },
  getPostsAside: (state, getters) => {
    const arr = [...getters.getFillPosts];
    return arr.reverse().filter((post, index) => index < 5);
  },
  getPostById: (state, getters) => (postId) => {
    return _.find(getters.getFillPosts, { route: postId }) || state.post
  },
  getFirstPost: (state) => {
    if (getters.getFillPosts.length) {
      return getters.getFillPosts[0];
    }
    return state.post;
  },
  getFirstCategory: (state, getters) => {
    return getters.getCategories[0] || [];
  },
  getTwoPostsByCategory: (state, getters) => (categoryId) => {
    const categoriesPosts = getters.getFillPosts.filter(({category: { id }}) => Number(categoryId) === Number(id));
    return categoriesPosts.filter((a, idx) => idx < 2);
  },
}

export const mutations = {
  updatePosts(state, data) {
    state.posts = [...data];
  }
}

export const actions = {};