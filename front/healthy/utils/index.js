export const generateRouteLink = name => {
  return name
    .toLowerCase()
    .split('')
    .map(x => {
      const re = (/([\.\!\?\,\'\"\(\)\{\}\:\;\s])/g);
      if (re.test(x)) {
        return '-';
      }
      return x;
    })
    .join('');
};