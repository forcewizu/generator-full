const shell = require('shelljs');
const fs = require('fs');
const path = require('path');
const axios = require('axios');
const archiver = require('archiver');
const fastify = require('fastify')({
  logger: true
});
const plugin = require('fastify-server-timeout');

const categories = {
  healthy: 15,
  newCategory: 21,
}

fastify.register(require('fastify-cors'), { 
  origin: "*",
  methods: ['GET', 'PUT', 'POST']
});

fastify.register(plugin, {
  serverTimeout: 1000 * 60 * 10,
})

fastify.register(require('fastify-static'), {
  root: path.join(__dirname, 'public'),
  prefix: '/public/', // optional: default '/'
})

fastify.get('/', async (request, reply) => {
  reply.type('application/json').code(200)
  return { hello: `world ${new Date()}` }
});

const fetchArticles = (template) => {
  const [ key, id ] = Object.entries(categories).find(([key, id]) => key.toLowerCase().includes(template.toLowerCase()))
  return new Promise((resolve, reject) => {
    const path = `./front/${template}/db.json`;
    axios.get(`http://104.248.178.17/api/get-articles?web=1&cat_id=${id}`)
    .then(({data}) => {
      const filtered = data.filter((item) => {
        return !!item.article && !!item.article.translate_text;
      })
      return filtered;
    })
    .then((result) => {
      const json = JSON.stringify(result);
      return json;
    })
    .then((json) => {
      fs.unlink(path, (err) => {
        if (err) {
          console.log(`Somthing wrong with removing db.json => ${err}`);
        } else {
          console.log(`db.json removed successfuly`);
        }
        fs.writeFile(path, json, 'utf8', () => {
          console.log('db.json compiled');
          setTimeout(() => resolve(), 4000);
        });
      });
    })
    .catch(e => reject(e)); 
  })
}

const generation = ({ TEMPLATE = '', MY_COLOR, MY_FONT, MY_FONT_SIZE, MY_NUMBER }) => {
  return new Promise((resolve, reject) => {
    const colorParams = MY_COLOR ? `MY_COLOR=${MY_COLOR} `: '';
    const fontParams = MY_FONT ? `MY_FONT='${MY_FONT}' `: '';
    const sizeParams = MY_FONT_SIZE ? `MY_FONT_SIZE=${MY_FONT_SIZE} `: '';
    const numberParams = MY_NUMBER ? `MY_NUMBER=${MY_NUMBER} `: '';
    shell.cd(`front/${TEMPLATE}`);
    shell.rm('-rf', 'start.sh');
    shell.exec('touch start.sh');
    shell.exec(`echo ${colorParams}${fontParams}${sizeParams}${numberParams} npm run generate >> start.sh`);
    shell.exec('sh start.sh');
    shell.cd(`../../`);
    setTimeout(() => resolve(), 1000);
  });
};

const archivation = (TEMPLATE) => {
  return new Promise((resolve, reject) => {
    const zipName = `dist${ Date.now() }`
    const output = fs.createWriteStream(__dirname + `/public/${zipName}.zip`);
    const archive = archiver('zip');
    const cur_dir = '/var/www/generator-full'
    output.on('close', function() {
      console.log(archive.pointer() + ' total bytes');
      console.log('archiver has been finalized and the output file descriptor has closed.');
      setTimeout(() => resolve(zipName), 1000);
    });
    output.on('end', function() {
      console.log('Data has been drained');
    });
    archive.on('error', function(err) {
      reject()
    });
    archive.pipe(output);
    archive.directory(`${cur_dir}/front/${TEMPLATE}/dist/`, false);
    archive.finalize();
  })
};

fastify.route({
  method: 'GET',
  url: '/template',
  schema: {
    querystring: {
      TEMPLATE: { type: 'string' },
      MY_COLOR: { type: 'string' },
      MY_FONT: { type: 'string' },
      MY_FONT_SIZE: { type: 'number' },
      MY_NUMBER: { type: 'number' },
    },
    response: {
      200: {
        type: 'object',
        properties: {
          message: { type: 'string' },
          code: { type: 'number' },
        }
      },
    }
  },
  handler: (request, reply) => {
    try {
      const { TEMPLATE = '' } = request.query;
      if (!TEMPLATE) {
        throw new Error();
      };
      fetchArticles(TEMPLATE)
        .then(() => {
          generation(request.query)
            .then(() => archivation(TEMPLATE))
            .then((zipName) => reply.send({ code: 200, message: `http://134.209.155.186/public/${zipName}.zip`}))
            .catch((err) => {
              reply.send({ code: 666, message: `Something wrong! ${err}` });
              console.error(err);
            })
        })
        .catch((er) => reply.send({ code: 404, message: `error ${er}` }));
    } catch (e) {
      reply.send({ code: 666, message: `Something wrong! ${e}` });
      console.error(e);
    }
  }
})

fastify.listen(3000, (err) => {
  if (err) throw err
  fastify.log.info(`server listening on ${fastify.server.address().post}`)
})